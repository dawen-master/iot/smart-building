from command import Command

BLIND_SET_FULL = 1  # type: int
BLIND_SET_VALUE = 3  # type: int
BLIND_GET_VALUE = 4  # type: int


class Blind:
    def __init__(self, floor: int, block: int):
        self.floor = floor
        self.block = block

    def command_set_full(self, full_close: bool):
        """
        Builds a command to completely open or close the blind.
        :param full_close: true to close completely, false to open completely.
        :return: Command to open or close the blind.
        :rtype: Command
        """
        data = 1 if full_close else 0
        command = Command(data=data, data_size=1, group_address="{0}/{1}/{2}".format(BLIND_SET_FULL, self.floor, self.block))
        return command

    def command_set_value(self, data: int):
        """
        Builds a command to set the aperture of the blind between 0 (open) and 255 (closed).
        :param data: Integer value between 0 (open) and 255 (closed).
        :return: Command to set the value of the blind.
        :rtype: Command
        """
        command = Command(data=data, data_size=2, group_address="{0}/{1}/{2}".format(BLIND_SET_VALUE, self.floor, self.block))
        return command

    def command_get_value(self):
        """
        Builds a command to get the value of the blind between 0 (open) and 255 (closed).
        :return: Command to get the value of the blind.
        :rtype: Command
        """
        command = Command(data=0, data_size=1, group_address="{0}/{1}/{2}".format(BLIND_GET_VALUE, self.floor, self.block))
        return command
