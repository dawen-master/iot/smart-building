define({ "api": [
  {
    "type": "post",
    "url": "/knx/<room_key>/<blind_key>/full",
    "title": "blind_full",
    "group": "Blinds",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"room1\"",
              "\"room2\""
            ],
            "optional": false,
            "field": "room_key",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"blind1\"",
              "\"blind2\""
            ],
            "optional": false,
            "field": "blind_key",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"open\"",
              "\"close\""
            ],
            "optional": false,
            "field": "body",
            "description": "<p>Body of the POST method</p>"
          }
        ]
      }
    },
    "description": "<p>Opens or closes completely a blind in a room.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "allowedValues": [
              "\"closed\"",
              "\"open\""
            ],
            "optional": false,
            "field": "200",
            "description": "<p>State of the blind</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.py",
    "groupTitle": "Blinds",
    "name": "PostKnxRoom_keyBlind_keyFull"
  },
  {
    "type": "get post",
    "url": "/knx/<room_key>/<facility_key>/facilities",
    "title": "get_set_facility_value",
    "group": "Facilities",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"room1\"",
              "\"room2\""
            ],
            "optional": false,
            "field": "room_key",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"blind1\"",
              "\"blind2\"",
              "\"valve1\"",
              "\"valve2\""
            ],
            "optional": false,
            "field": "facility_key",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Number",
            "size": "0-255",
            "optional": false,
            "field": "data",
            "description": "<p>Value passed by a POST request. JSON with key &quot;data&quot; or directly the value in the body.</p>"
          }
        ]
      }
    },
    "description": "<p>Gets or sets the value of a facility (blind, valve) in a room.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "size": "0-255",
            "optional": false,
            "field": "200",
            "description": "<p>Current value of the facility</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.py",
    "groupTitle": "Facilities",
    "name": "Get_postKnxRoom_keyFacility_keyFacilities"
  },
  {
    "type": "get",
    "url": "/knx/<room_key>/facilities",
    "title": "get_facility_keys",
    "group": "Rooms",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "allowedValues": [
              "\"room1\"",
              "\"room2\""
            ],
            "optional": false,
            "field": "room_key",
            "description": ""
          }
        ]
      }
    },
    "description": "<p>Request the IDs of the facilities in a room.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "200",
            "description": "<p>List of facilities IDs (blinds, valves).</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.py",
    "groupTitle": "Rooms",
    "name": "GetKnxRoom_keyFacilities"
  },
  {
    "type": "get",
    "url": "/knx/rooms",
    "title": "get_room_keys",
    "group": "Rooms",
    "description": "<p>Request the IDs of all rooms.</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "200",
            "description": "<p>List of rooms ID</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.py",
    "groupTitle": "Rooms",
    "name": "GetKnxRooms"
  }
] });
