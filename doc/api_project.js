define({
  "name": "KNX REST API",
  "version": "0.1.0",
  "description": "KNX REST API documentation",
  "title": "KNX Rest Server",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2018-10-31T21:50:15.391Z",
    "url": "http://apidocjs.com",
    "version": "0.17.6"
  }
});
