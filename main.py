# -*- coding: utf-8 -*-
import socket

from knxnet import *

gateway_ip = "0.0.0.0"  # "IP address KNXnet/IP gateway"
gateway_port = 3671  # "Listening Port"
# -> in this example, for sake of simplicity, the two ports are the same
# With the simulator, the gateway_ip must be set to 127.0.0.1 and gateway_port to 3671
data_endpoint = ('0.0.0.0', 3671)
control_enpoint = ('0.0.0.0', 3671)
object_addr = '0/4/1'
# -> Socket creation
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(('', 3672))

#                                   CREATE CONNECTION
conn_req_object = knxnet.create_frame(knxnet.ServiceTypeDescriptor.CONNECTION_REQUEST, control_enpoint,
                                      data_endpoint)
conn_req_dtgrm = conn_req_object.frame  # -> Serializing
sock.sendto(conn_req_dtgrm, (gateway_ip, gateway_port))
# <- Receiving Connection response
data_recv, addr = sock.recvfrom(1024)
conn_resp_object = knxnet.decode_frame(data_recv)
# <- Retrieving channel_id from Connection response
conn_channel_id = conn_resp_object.channel_id

# Connection State Request
conn_stat_req = knxnet.create_frame(knxnet.ServiceTypeDescriptor.CONNECTION_STATE_REQUEST, conn_channel_id,
                                    control_enpoint)
conn_stat_req_dtgrm = conn_stat_req.frame
sock.sendto(conn_stat_req_dtgrm, (gateway_ip, gateway_port))

# Connection state Response
data_recv, addr = sock.recvfrom(1024)
conn_stat_resp = knxnet.decode_frame(data_recv)
#                                   END CONNECTION

#                                   REQUEST
# Tunneling request
tunn_req = knxnet.create_frame(knxnet.ServiceTypeDescriptor.TUNNELLING_REQUEST,
                               knxnet.GroupAddress.from_str(object_addr),
                               conn_channel_id, 0, 1, 0x0)
tunn_req_dtgrm = tunn_req.frame
sock.sendto(tunn_req_dtgrm, (gateway_ip, gateway_port))

# Tunneling Ack Response
data_recv, addr = sock.recvfrom(1024)
tunn_req_ack = knxnet.decode_frame(data_recv)

# Tunneling request Response
data_recv, addr = sock.recvfrom(1024)
tunn_req_resp = knxnet.decode_frame(data_recv)

# Tunneling Ack Send
tunn_ack = knxnet.create_frame(knxnet.ServiceTypeDescriptor.TUNNELLING_ACK, conn_channel_id, 0,
                               tunn_req.sequence_counter)
sock.sendto(tunn_ack.frame, (gateway_ip, gateway_port))

# Tunneling request Response With content
data_recv, addr = sock.recvfrom(1024)
tunn_req_resp = knxnet.decode_frame(data_recv)

print("value {}".format(tunn_req_resp.data))
#                                   END REQUEST

#                                   DISCONNECT
# Send disconnect request
disc_req = knxnet.create_frame(knxnet.ServiceTypeDescriptor.DISCONNECT_REQUEST, conn_channel_id, control_enpoint)
sock.sendto(disc_req.frame, (gateway_ip, gateway_port))

# receive disconnect response
data_recv, addr = sock.recvfrom(1024)
tunn_req_resp = knxnet.decode_frame(data_recv)
if 0 == tunn_req_resp.status:
    print("Successfully disconnected")
else:
    print("Error during disconnect: {}".format(tunn_req_resp.data))
