import socket
from enum import IntEnum
from typing import Tuple

from knxnet import *

from command import Command

class ApciFlag(IntEnum):
    """
    APCI (Application Layer Protocol Control Information)
    """
    READ_QUERY = 0x0  # Sent by the client
    RESPONSE_TO_QUERY = 0x1  # In case of Tunnelling request sent by gateway following a Tunnelling request sent by the client.
    WRITE_QUERY = 0x2  # Sent by the client


class AckStatus(IntEnum):
    SUCCESS = 0x0,
    FAIL = 0x1,


class Connection:
    # Connection parameters
    # "IP address KNXnet/IP gateway"
    gateway_ip = "0.0.0.0"  # type: str
    # "Listening Port"
    gateway_port = 3671  # type: int
    # Connection/disconnection HPAI (IP address, port number) (Host Protocol Address Information)
    control_endpoint = ('0.0.0.0', 3671)  # type: Tuple[str, int]
    # Tunnelling HPAI (IP address, port number) (Host Protocol Address Information)
    data_endpoint = ('0.0.0.0', 3671)  # type: Tuple[str, int]
    # Channel identifier allocated by the KNX/IP gateway for the client.
    conn_channel_id = None  # type: str
    # Socket
    sock = None  # type: socket.socket

    def __init__(self):
        pass

    def __enter__(self):
        # Open socket
        if self.sock is None:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.sock.bind(('', 3672))
        #                                   CREATE CONNECTION
        conn_req_resp = self.do_connection_request()
        # <- Retrieving channel_id from connection request response
        self.conn_channel_id = conn_req_resp.channel_id

        self.do_connection_state_request()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.do_disconnection_request()

    @staticmethod
    def read_group_address(group_address: str):
        if isinstance(group_address, str):
            connection = Connection()
            with connection as live_connection:
                response = live_connection.do_tunnel_read(group_address)
                if isinstance(response, knxnet.TunnellingRequest):
                    return response.data

    @staticmethod
    def read_command(command: Command):
        if isinstance(command, Command):
            return Connection.read_group_address(command.group_address)

    @staticmethod
    def write_command(command: Command):
        if isinstance(command, Command):
            connection = Connection()
            with connection as live_connection:
                live_connection.do_tunnel_write(command.group_address, command.data, command.data_size)

    def read_value(self, object_addr: str) -> knxnet.TunnellingRequest:

        # Open socket
        if self.sock is None:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            self.sock.bind(('', 3672))
        #                                   CREATE CONNECTION
        conn_req_resp = self.do_connection_request()
        # <- Retrieving channel_id from connection request response
        self.conn_channel_id = conn_req_resp.channel_id

        self.do_connection_state_request()
        #                                   END CONNECTION

        #                                   REQUEST
        value_response = self.do_tunnel_read(object_addr)
        #                                   END REQUEST

        self.do_disconnection_request()

        return value_response

    def do_connection_request(self) -> knxnet.ConnectionResponse:
        """
        Sends a Connection Request and waits for a response.
        """
        conn_req = knxnet.create_frame(knxnet.ServiceTypeDescriptor.CONNECTION_REQUEST,
                                       self.control_endpoint,
                                       self.data_endpoint)
        self.send_datagram(conn_req.frame)

        conn_req_resp = self.receive_response()

        return conn_req_resp

    def do_connection_state_request(self) -> knxnet.ConnectionStateResponse:
        """
        Sends a Connection State Request and waits for a response.
        """
        conn_state_req = knxnet.create_frame(knxnet.ServiceTypeDescriptor.CONNECTION_STATE_REQUEST,
                                             self.conn_channel_id, self.control_endpoint)
        self.send_datagram(conn_state_req.frame)

        conn_state_req_resp = self.receive_response()

        return conn_state_req_resp

    def do_disconnection_request(self) -> knxnet.DisconnectResponse:
        """
        Sends a Disconnection Request and waits for a response.
        """
        disc_req = knxnet.create_frame(knxnet.ServiceTypeDescriptor.DISCONNECT_REQUEST,
                                       self.conn_channel_id,
                                       self.control_endpoint)
        self.send_datagram(disc_req.frame)

        disc_req_resp = self.receive_response()

        if 0 == disc_req_resp.status:
            print("Successfully disconnected")
        else:
            print("Error during disconnect: {}".format(disc_req_resp.data))

        return disc_req_resp

    def do_tunnel_read(self, group_address: str) -> knxnet.TunnellingRequest:
        """
        Initiates a tunnel and reads the value of an object located at object_addr.
        """
        # Tunneling request
        tunn_req = knxnet.create_frame(knxnet.ServiceTypeDescriptor.TUNNELLING_REQUEST,
                                       knxnet.GroupAddress.from_str(group_address),
                                       self.conn_channel_id, 0, 1,
                                       ApciFlag.READ_QUERY)  # type: knxnet.TunnellingRequest
        tunn_req_dtgrm = tunn_req.frame
        self.send_datagram(tunn_req_dtgrm)

        # Tunneling Ack response
        self.receive_response()  # type: knxnet.TunnellingAck

        # Tunneling Request response
        self.receive_response()  # type: knxnet.TunnellingRequest

        # Tunneling Ack send
        # For acknowledgement, the sequence counter has to be the same as the one sent in the tunneling request.
        tunn_ack = knxnet.create_frame(knxnet.ServiceTypeDescriptor.TUNNELLING_ACK, self.conn_channel_id,
                                       AckStatus.SUCCESS,
                                       tunn_req.sequence_counter)  # type: knxnet.TunnellingAck
        self.send_datagram(tunn_ack.frame)

        # Tunneling request Response With content
        tunn_req_content = self.receive_response()  # type: knxnet.TunnellingRequest

        if isinstance(tunn_req_content, knxnet.TunnellingRequest):
            print("Reading {0}[{1}] to {2}".format(tunn_req_content.data, tunn_req_content.data_size, group_address))
        else:
            print("Cannot read from {0}".format(group_address))

        return tunn_req_content

    def do_tunnel_write(self, group_address: str, data: int, data_size: int):
        """
        Initiates a tunnel and writes a value to the object located at group_address.

        Attributes
        ----------
        group_address : str
            Group address of the object to write to in the form 'action/floor/block'.
        data: int
            Value to write. Can be 1-byte or 2-bytes long.
        data_size: Length (in bytes) of the data to write, usually 1 or 2.
        """

        print("Writing {0}[{1}] to {2}".format(data, data_size, group_address))

        tunn_req = knxnet.create_frame(knxnet.ServiceTypeDescriptor.TUNNELLING_REQUEST,
                                       knxnet.GroupAddress.from_str(group_address),
                                       self.conn_channel_id, data, data_size,
                                       ApciFlag.WRITE_QUERY)  # type: knxnet.TunnellingRequest
        tunn_req_dtgrm = tunn_req.frame
        self.send_datagram(tunn_req_dtgrm)

        # Tunneling Ack response
        tunn_ack_recv = self.receive_response()  # type: knxnet.TunnellingAck

        # Tunneling Request response
        tunn_req_recv = self.receive_response()  # type: knxnet.TunnellingRequest

        # Tunneling Ack send
        # For acknowledgement, the sequence counter has to be the same as the one sent in the tunneling request.
        tunn_ack = knxnet.create_frame(knxnet.ServiceTypeDescriptor.TUNNELLING_ACK, self.conn_channel_id,
                                       AckStatus.SUCCESS,
                                       tunn_req.sequence_counter)  # type: knxnet.TunnellingAck
        self.send_datagram(tunn_ack.frame)

    def receive_response(self):
        data_recv, addr = self.sock.recvfrom(1024)
        response = knxnet.decode_frame(data_recv)
        return response

    def send_datagram(self, datagram: bytearray):
        self.sock.sendto(datagram, (self.gateway_ip, self.gateway_port))

    def __del__(self):
        if self.sock is not None:
            # Free the socket.
            self.sock.close()
