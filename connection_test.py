from connection import Connection
from blind import Blind
from valve import Valve


def percent_to_octet(percent: int):
    """
    Converts a value between 0 and 255 into a percentage (integer) between 0 and 100.
    :param percent: value between 0 and 255.
    :return: Percent (integer) between 0 and 100.
    :rtype: int
    """
    if percent < 0:
        return 0
    elif percent > 255:
        return 255
    else:
        return int(round((percent * 255) / 100))


def octet_to_percent(octet: int):
    """
    Converts a value between 0 and 255 into a percentage (integer) between 0 and 100.
    :param octet: value between 0 and 255.
    :return: Percent (integer) between 0 and 100.
    :rtype: int
    """
    if octet < 0:
        return 0
    elif octet > 255:
        return 100
    else:
        return int(round((octet * 100) / 255))


room1_dict = {
    "blinds": [Blind(4, 1), Blind(4, 2)],
    "valves": [Valve(4, 1), Valve(4, 2)]
}

room2_dict = {
    "blinds": [Blind(4, 10), Blind(4, 11)],
    "valves": [Valve(4, 10), Valve(4, 11)]
}

rooms_dict = {
    "room1": room1_dict,
    "room2": room2_dict
}


def demo_value(data: int):
    if data <= 255-16:
        return data + 16
    else:
        return 0


value_room1_blind1 = Connection.read_command(rooms_dict["room1"]["blinds"][0].command_get_value())
value_room1_blind2 = Connection.read_command(rooms_dict["room1"]["blinds"][1].command_get_value())
value_room2_blind1 = Connection.read_command(rooms_dict["room2"]["blinds"][0].command_get_value())
value_room2_blind2 = Connection.read_command(rooms_dict["room2"]["blinds"][1].command_get_value())

value_room1_valve1 = Connection.read_command(rooms_dict["room1"]["valves"][0].command_get_value())
value_room1_valve2 = Connection.read_command(rooms_dict["room1"]["valves"][1].command_get_value())
value_room2_valve1 = Connection.read_command(rooms_dict["room2"]["valves"][0].command_get_value())
value_room2_valve2 = Connection.read_command(rooms_dict["room2"]["valves"][1].command_get_value())

blind_closed = True
Connection.write_command(rooms_dict["room1"]["blinds"][0].command_set_full(blind_closed))
Connection.write_command(rooms_dict["room1"]["blinds"][1].command_set_full(blind_closed))
Connection.write_command(rooms_dict["room2"]["blinds"][0].command_set_full(blind_closed))
Connection.write_command(rooms_dict["room2"]["blinds"][1].command_set_full(blind_closed))

Connection.write_command(rooms_dict["room1"]["blinds"][0].command_set_value(demo_value(value_room1_blind1)))
Connection.write_command(rooms_dict["room1"]["blinds"][1].command_set_value(demo_value(value_room1_blind2)))
Connection.write_command(rooms_dict["room2"]["blinds"][0].command_set_value(demo_value(value_room2_blind1)))
Connection.write_command(rooms_dict["room2"]["blinds"][1].command_set_value(demo_value(value_room2_blind2)))

Connection.write_command(rooms_dict["room1"]["valves"][0].command_set_value(demo_value(value_room1_valve1)))
Connection.write_command(rooms_dict["room1"]["valves"][1].command_set_value(demo_value(value_room1_valve2)))
Connection.write_command(rooms_dict["room2"]["valves"][0].command_set_value(demo_value(value_room2_valve1)))
Connection.write_command(rooms_dict["room2"]["valves"][1].command_set_value(demo_value(value_room2_valve2)))


# connection = Connection()
#
# # Change address and value for read or write
# with connection as live_connection:
#     req_rsp = live_connection.do_tunnel_read('0/4/1')
#     print("Value received : {}".format(req_rsp.data))
#     req_rsp = live_connection.do_tunnel_read('0/4/2')
#     print("Value received : {}".format(req_rsp.data))
#     write_rsp = live_connection.do_tunnel_write('3/4/2', 201, 2)
#     write_rsp = live_connection.do_tunnel_write('0/4/1', 127, 2)
#     print("Finished with live connection")

print("Finished")
