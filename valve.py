from command import Command

VALVE_GET_SET = 0  # type: int
VALVE_CLOSE = 0  # type: int
VALVE_OPEN = 255  # type: int

class Valve:
    def __init__(self, floor: int, block: int):
        self.floor = floor
        self.block = block

    def command_get_value(self):
        """
        Builds a command to get the value of the valve between 0 (closed) and 255 (open).
        :return: Command to get the value of the valve.
        :rtype: Command
        """
        command = Command(data=0, data_size=1, group_address="{0}/{1}/{2}".format(VALVE_GET_SET, self.floor, self.block))
        return command

    def command_set_value(self, data: int):
        """
        Builds a command to set the value of the valve between 0 (closed) and 255 (open).
        :param data: Integer value between 0 (closed) and 255 (open).
        :return: Command to set the value of the valve.
        :rtype: Command
        """
        command = Command(data=data, data_size=2, group_address="{0}/{1}/{2}".format(VALVE_GET_SET, self.floor, self.block))
        return command
