import flask
from flask import Flask
from hepia_building import hepia_rooms_dict
from connection import Connection
from blind import Blind

app = Flask(__name__)

FACILITY_DATA_KEY = 'data' # type:str


def find_facility(room_key: str, facility_key: str):
    try:
        room = hepia_rooms_dict[room_key]
        try:
            facility = room[facility_key]
            return facility
        except KeyError as ex:
            flask.abort(404, "Cannot find facility '{0}'.".format(facility_key))
    except KeyError as ex:
        flask.abort(404, "Cannot find room '{0}'.".format(room_key))


def set_blind_full(room_key: str, blind_key: str, full_close: bool):
    facility = find_facility(room_key, blind_key)
    if isinstance(facility, Blind):
        blind = facility  # type: Blind
        command = blind.command_set_full(full_close)
        Connection.write_command(command)
        return "closed" if full_close else "opened"
    else:
        flask.abort(400, "Full open/close command works only with blinds.")


@app.route("/")
def hello():
    return "Hello World!"


@app.route('/knx/rooms', methods=['GET'])
def get_room_keys():
    """
    @api {get} /knx/rooms get_room_keys
    @apiGroup Rooms
    @apiDescription Request the IDs of all rooms.
    @apiSuccess {String[]} 200 List of rooms ID
    """
    return flask.jsonify(list(hepia_rooms_dict.keys()))


@app.route('/knx/<string:room_key>/facilities', methods=['GET'])
def get_facility_keys(room_key: str):
    """
    @api {get} /knx/<room_key>/facilities get_facility_keys
    @apiGroup Rooms
    @apiParam {String="room1","room2"} room_key
    @apiDescription Request the IDs of the facilities in a room.
    @apiSuccess {String[]} 200 List of facilities IDs (blinds, valves).
    """
    try:
        return flask.jsonify(list(hepia_rooms_dict[room_key.lower()]))
    except KeyError as ex:
        flask.abort(404, "Cannot find room '{0}'.".format(room_key))


@app.route('/knx/<string:room_key>/<string:facility_key>/value', methods=['GET', 'POST'])
def get_set_facility_value(room_key: str, facility_key: str):
    """
    @api {get post} /knx/<room_key>/<facility_key>/facilities get_set_facility_value
    @apiGroup Facilities
    @apiParam {String="room1","room2"} room_key
    @apiParam {String="blind1","blind2","valve1","valve2"} facility_key
    @apiParam {Number{0-255}} data Value passed by a POST request. JSON with key "data"
    or directly the value in the body.
    @apiDescription Gets or sets the value of a facility (blind, valve) in a room.
    @apiSuccess {Number{0-255}} 200 Current value of the facility
    """
    facility = find_facility(room_key.lower(), facility_key.lower())

    if flask.request.method == 'GET':
        command = facility.command_get_value()
        response = Connection.read_command(command)
        return flask.jsonify(response)

    elif flask.request.method == 'POST':

        raw_data = None

        if flask.request.mimetype == 'application/json':
            # Find key 'data'.
            try:
                raw_data = flask.request.get_json(force=True).get(FACILITY_DATA_KEY)
            except:
                raw_data = None

            if raw_data is None:
                flask.abort(400, "Cannot find key '{0}' in request.".format(FACILITY_DATA_KEY))

        elif flask.request.mimetype == 'text/plain':
            try:
                raw_data = int(flask.request.data)
            except:
                raw_data = None
        else:
            flask.abort(400, "MIME type must be 'application/json' or 'text/plain'. '{0}' is not supported."
                        .format(flask.request.mimetype))

        # Get the integer value.
        try:
            data = int(raw_data)
        except:
            flask.abort(400, "Value must be an integer.")

        if data < 0 or data > 255:
            flask.abort(400, "Value must be between 0 and 255 (inclusive).")

        # Send the command to the KNX network.
        command = facility.command_set_value(data)
        Connection.write_command(command)

        # Return the data (with HTTP status code 200) to indicate success.
        return flask.jsonify(data)


@app.route('/knx/<string:room_key>/<string:blind_key>/full', methods=['POST'])
def blind_full(room_key: str, blind_key: str):
    """
    @api {post} /knx/<room_key>/<blind_key>/full blind_full
    @apiGroup Blinds
    @apiParam {String="room1","room2"} room_key
    @apiParam {String="blind1","blind2"} blind_key
    @apiParam {String="open","close"} body Body of the POST method
    @apiDescription Opens or closes completely a blind in a room.
    @apiSuccess {String="closed","open"} 200 State of the blind
    """
    if flask.request.mimetype == 'text/plain':
        close = None
        try:
            text = str(flask.request.get_data(as_text=True)).lower()

            if text == "open" or text == "0":
                close = False
            elif text == "close" or text == "1":
                close = True
        except:
            close = None

        if close is not None:
            return set_blind_full(room_key, blind_key, close)
        else:
            flask.abort(400, "Data must be 'open'/'0' or 'close'/'1' (case insensitive).")
    else:
        flask.abort(400, "MIME type must be 'text/plain'. '{0}' is not supported."
                    .format(flask.request.mimetype))


if __name__ == "__main__":
    app.run()