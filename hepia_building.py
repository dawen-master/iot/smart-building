from blind import Blind
from valve import Valve

hepia_room1_dict = {
    'blind1': Blind(4, 1),
    'blind2': Blind(4, 2),
    'valve1': Valve(4, 1),
    'valve2': Valve(4, 2)
}

hepia_room2_dict = {
    'blind1': Blind(4, 10),
    'blind2': Blind(4, 11),
    'valve1': Valve(4, 10),
    'valve2': Valve(4, 11)
}

hepia_rooms_dict = {
    "room1": hepia_room1_dict,
    "room2": hepia_room2_dict
}